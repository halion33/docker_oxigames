<?php

namespace Tests\App\Controller\Api;

use App\Tests\ApiTestCase;
use App\Tests\BGGApiTestCase;
use GuzzleHttp\Client;

class TokenControllerTest extends BGGApiTestCase
{
    protected function setUp(): void
    {
        //@todo
        $this->markTestSkipped(
            'Old test. Needs to be refactored.'
        );
    }

    public function testPOSTCreateToken()
    {
        $user = $this->createUser('oscar_test', '123456');
        $this->printDebug($user->isEnabled());


        $response = $this->client->post('index_test.php/api/token', [
            'auth' => ['oscar_test', '123456']
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->asserter()->assertResponsePropertyExists(
            $response,
            'token'
        );
    }

    public function testPOSTTokenInvalidCredentials()
    {
        $this->createUser('oscar_test', '123456');
        $response = $this->client->post('index_test.php/api/token', [
            'auth' => ['oscar', 'invalid_password']
        ]);
        $this->printDebug($response->getBody());
        $this->assertContains('wrong credentials', json_decode($response->getBody(), true));
    }

}