<?php

namespace Tests\Acceptance;

use App\Tests\BGGApiTestCase;
use OGO\Domain\User\User;
use OGO\Application\Message\CollectionToImportMessage;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Messenger\Worker;
use Symfony\Component\Messenger\EventListener;


class BggImportationTest extends BGGApiTestCase
{
    protected $gameRepository;

    /**@var \Symfony\Component\Messenger\MessageBusInterface $bus */
    protected $bus;

    /**@var \Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransport $doctrineTransport */
    private $doctrineTransport;

    /**@var LoggerInterface $logger*/
    private $logger;

    protected function setUp() : void
    {
        self::bootKernel();
        parent::setUp();
        $this->gameRepository = self::$container->get('OGO\Infrastructure\Persistence\Doctrine\GameRepository');
        $this->bus = self::$container->get('Symfony\Component\Messenger\MessageBusInterface');
        $this->doctrineTransport= self::$container->get('messenger.transport.async');
        $this->logger = self::$container->get(LoggerInterface::class);
    }

    /** @test */
    public function importationTest(){
        //Here we can use a Dummy message and a Dummy receiver. https://github.com/symfony/messenger/blob/master/Tests/WorkerTest.php
        $consoleApplication = new Application(self::$kernel);
        $importationCommand = $consoleApplication->find('oxigames:import-bgg-collection');
        $commandTester = new CommandTester($importationCommand);

        $user = $this->createUser('oxieva');

        $commandTester->execute([
            'BggUserIdentifier' => $user->getBggIdentifier(),
        ]);

        $this->assertEquals(1, $this->doctrineTransport->getMessageCount());

        $eventDispatcher = new EventDispatcher();
        $eventDispatcher->addSubscriber(new EventListener\StopWorkerOnMessageLimitListener( 1, $this->logger));

        $worker = new Worker([$this->doctrineTransport], $this->bus, $eventDispatcher, $this->logger);
        $worker->run([]);
    }

}