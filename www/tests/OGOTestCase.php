<?php

namespace App\Tests;

use OGO\Domain\User\User;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\PropertyAccess\PropertyAccess;

abstract class OGOTestCase extends KernelTestCase
{
    private static $staticClient;

    /**
     * @var array
     */
    private static $historyContainer;

    private static $stack;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var FormatterHelper
     */
    private $formatterHelper;

    /**
     * @var ConsoleOutput
     */
    private $output;

    /**
     * @var ResponseAsserter
     */
    private $responseAsserter;

    private $projectDir;

    public static function setUpBeforeClass() : void
    {
        self::$historyContainer = array();
        $history = Middleware::history(self::$historyContainer);

        self::$stack = HandlerStack::create();
        self::$stack->push($history);

        self::$staticClient = new Client([
            'base_uri' => $_ENV['BASE_URL'],
            'http_errors' => false,
            'exceptions' => false,
            'handler' => self::$stack,
            'verify' => false
        ]);

        self::$stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $uri = $request->getUri()->getPath();
            if (strpos($uri, '/api') === 0 && strpos($uri, '/index_test.php') === false) {
                $request = $request->withUri($request->getUri()->withPath('/index_test.php' . $uri));
            }

            return $request;
        }));

        self::bootKernel();
    }

    protected function setUp() : void
    {
        $this->client = self::$staticClient;
        $this->projectDir = self::$container->getParameter('kernel.project_dir');
        $this->purgeDatabase();
    }

    /**
     * Clean up Kernel usage in this test.
     */
    protected function tearDown() : void
    {
    }

    protected function onNotSuccessfulTest(\Throwable $e) :void
    {
        $transaction = $this->getLastResponse();

        if ($transaction) {
            $this->printDebug('');
            $this->printDebug('<error>Failure!</error> when making the following request:');
            $this->printLastRequestUrl();
            $this->printDebug('');
            $this->debugResponse($transaction);
        }

        throw $e;
    }

    private function purgeDatabase()
    {
        $em = $this->getEntityManager();
        $purger = new ORMPurger($em);
        $purger->purge();
        $sql = 'TRUNCATE messenger_messages';
        $connection = $em->getConnection();
        $stmt = $connection->prepare($sql);
        $stmt->execute();
        $stmt->closeCursor();
    }

    protected function getService($id)
    {
        return self::$kernel->getContainer()
            ->get($id);
    }

    protected function printLastRequestUrl()
    {
        //$lastRequest = self::$history->getLastRequest();
        /** @var RequestInterface $lastRequest */
        $lastRequest = $this->getLastResponse()['request'];

        if ($lastRequest) {
            $this->printDebug(sprintf('<comment>%s</comment>: <info>%s</info>', $lastRequest->getBody(), $lastRequest->getUri()->getPath()));
        } else {
            $this->printDebug('No request was made.');
        }
    }

    protected function debugResponse(array $transaction)
    {
        /** @var RequestInterface $request */
        $request = $transaction['request'];
        /** @var ResponseInterface $response */
        $response = $transaction['response'];
        if(!$response){
            $this->printDebug( 'failed request');
        }
        //$this->printDebug(AbstractMessage::getStartLineAndHeaders($response));
        $body = (string) $response->getBody();

        $contentType = $response->getHeader('Content-Type')[0];
        if ($contentType == 'application/json' || strpos($contentType, '+json') !== false) {
            $data = json_decode($body);
            if ($data === null) {
                // invalid JSON!
                $this->printDebug($body);
            } else {
                // valid JSON, print it pretty
                $this->printDebug(json_encode($data, JSON_PRETTY_PRINT));
            }
        } else {
            // the response is HTML - see if we should print all of it or some of it
            $isValidHtml = strpos($body, '</body>') !== false;

            if ($isValidHtml) {
                $this->printDebug('');
                $crawler = new Crawler($body);

                // very specific to Symfony's error page
                $isError = $crawler->filter('#traces-0')->count() > 0
                    || strpos($body, 'looks like something went wrong') !== false;
                if ($isError) {
                    $this->printDebug('There was an Error!!!!');
                    $this->printDebug('');
                } else {
                    $this->printDebug('HTML Summary (h1 and h2):');
                }

                // finds the h1 and h2 tags and prints them only
                foreach ($crawler->filter('h1, h2')->extract(array('_text')) as $header) {
                    // avoid these meaningless headers
                    if (strpos($header, 'Stack Trace') !== false) {
                        continue;
                    }
                    if (strpos($header, 'Logs') !== false) {
                        continue;
                    }

                    // remove line breaks so the message looks nice
                    $header = str_replace("\n", ' ', trim($header));
                    // trim any excess whitespace "foo   bar" => "foo bar"
                    $header = preg_replace('/(\s)+/', ' ', $header);

                    if ($isError) {
                        $this->printErrorBlock($header);
                    } else {
                        $this->printDebug($header);
                    }
                }

                /*
                 * When using the test environment, the profiler is not active
                 * for performance. To help debug, turn it on temporarily in
                 * the config_test.yml file:
                 *   A) Update framework.profiler.collect to true
                 *   B) Update web_profiler.toolbar to true
                 */
                $profilerUrl = $response->getHeader('X-Debug-Token-Link');
                if ($profilerUrl) {
                    $fullProfilerUrl = $response->getHeader('Host').$profilerUrl;
                    $this->printDebug('');
                    $this->printDebug(sprintf(
                        'Profiler URL: <comment>%s</comment>',
                        $fullProfilerUrl
                    ));
                }

                // an extra line for spacing
                $this->printDebug('');
            } else {
                $this->printDebug($body);
            }
        }
    }

    /**
     * Print a message out - useful for debugging
     *
     * @param $string
     */
    protected function printDebug($string)
    {
        if ($this->output === null) {
            $this->output = new ConsoleOutput();
        }

        $this->output->writeln($string);
    }

    /**
     * Print a debugging message out in a big red block
     *
     * @param $string
     */
    protected function printErrorBlock($string)
    {
        if ($this->formatterHelper === null) {
            $this->formatterHelper = new FormatterHelper();
        }
        $output = $this->formatterHelper->formatBlock($string, 'bg=red;fg=white', true);

        $this->printDebug($output);
    }

    /**
     * @param $username
     * @param string $plainPassword
     * @return User
     */
    protected function createUser($username, $plainPassword = 'foo')
    {
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($username.'@foo.com');
        $password = $this->getService('security.password_encoder')
            ->encodePassword($user, $plainPassword);
        $user->setPassword($password);
        $user->setbggIdentifier($username);
        $user->setEnabled('true');
        $user->setRoles(array('admin'));
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        return $user;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getService('doctrine')
            ->getManager();
    }

    /**
     * @return ResponseAsserter
     */
    protected function asserter()
    {
        if ($this->responseAsserter === null) {
            $this->responseAsserter = new ResponseAsserter();
        }
        return $this->responseAsserter;
    }

    private function getLastResponse()
    {
        if ( (is_array(self::$historyContainer) || self::$historyContainer instanceof \Traversable) && count(self::$historyContainer) > 0) {
            $transaction = end(self::$historyContainer);
            return $transaction;
        }
    }

    /**
     * Call this when you want to compare URLs in a test
     *
     * (since the returned URL's will have /app_test.php in front)
     *
     * @param string $uri
     * @return string
     */
    protected function adjustUri($uri)
    {
        return '/index_test.php'.$uri;
    }

    /**
     * @param $username
     * @param array $headers
     * @return array
     */
    protected function getAuthorizedHeaders($username, $headers = array())
    {
        $token = $this->getService('lexik_jwt_authentication.encoder')
            ->encode(['username' => $username]);

        $headers['Authorization'] = 'Bearer '.$token;

        return $headers;
    }

    public function getFakeDataDirectory(){
        return $this->projectDir . '/src/FakeData';
    }
}
