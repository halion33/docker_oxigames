<?php

namespace App\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\ConsoleOutput;

class BGGApiTestCase extends OGOTestCase
{
    private static $staticClient;

    /**
     * @var array
     */
    private static $historyContainer;

    private static $stack;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var FormatterHelper
     */
    private $formatterHelper;

    /**
     * @var ConsoleOutput
     */
    private $output;

    /**
     * @var ResponseAsserter
     */
    private $responseAsserter;

    public static function setUpBeforeClass() :void
    {
        self::$historyContainer = array();
        $history = Middleware::history(self::$historyContainer);

        self::$stack = HandlerStack::create();
        self::$stack->push($history);

        self::$staticClient = new Client([
            'base_uri' => $_ENV['BGG_API_URL'],
            'http_errors' => false,
            'exceptions' => false,
            'handler' => self::$stack,
            'verify' => false
        ]);

        self::$stack->push(Middleware::mapRequest(function (RequestInterface $request) {
            $uri = $request->getUri()->getPath();
            if (strpos($uri, '/api') === 0 && strpos($uri, '/index_test.php') === false) {
                $request = $request->withUri($request->getUri()->withPath('/index_test.php' . $uri));
            }

            return $request;
        }));

        self::bootKernel();
    }

    protected function setUp() : void
    {
        parent::setUp();
    }
}
