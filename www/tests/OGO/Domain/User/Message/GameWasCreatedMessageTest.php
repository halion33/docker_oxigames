<?php

namespace Tests\Domain\User\Message;

use App\Tests\OGOTestCase;
use OGO\Domain\Game\Game;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransport;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;

class GameWasCreatedMessageTest extends OGOTestCase {

    /**@var OGO\Infrastructure\Persistence\Doctrine\GameRepository $gameRepository */
    private $gameRepository;

    /**@var EntityManagerInterface $em*/
    private $em;

    protected function setUp() : void
    {
        self::bootKernel();
        parent::setUp();
        $this->gameRepository  = self::$container->get(GameRepository::class);
        $this->em               = self::$container->get('Doctrine\ORM\EntityManagerInterface');
    }

    public function testGameWasCreatedMessage(){
        $game = new Game();
        $game->setName('targi');
        $game->setDescription('best game in the world');

        $user = $this->createUser('oscar');

        try{
            $this->em->persist($game);
            $this->em->flush();
        }
        catch (\Exception $exception){
            $this->assertStringContainsString('cannot be saved directly. Use the save method', $exception->getMessage());
        }

        $this->gameRepository->save($game, $user);
        $this->assertEquals('targi', $game->getName());
        $this->assertIsInt($game->getId());
    }
}