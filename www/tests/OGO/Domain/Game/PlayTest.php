<?php

namespace Tests\Domain\Game;

use App\Tests\OGOTestCase;
use OGO\Domain\Game\Game;
use OGO\Domain\Game\Play;
use OGO\Domain\Game\PlayRepositoryInterface;
use OGO\Domain\User\UserProfile;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransport;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;

class PlayTest extends OGOTestCase {

    /**@var GameRepository $gameRepository */
    private $gameRepository;

    /**@var PlayRepositoryInterface $playRespsitory */
    private $playRepository;

    /**@var EntityManagerInterface $em*/
    private $em;

    protected function setUp() : void
    {
        self::bootKernel();
        parent::setUp();
        $this->playRepository  = self::$container->get(PlayRepositoryInterface::class);
        $this->gameRepository  = self::$container->get(GameRepository::class);
        $this->em               = self::$container->get('Doctrine\ORM\EntityManagerInterface');
    }

    /** @test */
    public function playAndUserProfileHasCorrectRelations(){
        $game = new Game();
        $game->setName('targi');
        $game->setDescription('best game in the world');

        $user = $this->createUser('oscar');
        $user2 = $this->createUser('oxieva');

        /*@todo, delete this when we have the domain message UserWasCreated,
           where we add this relation when the user is created*/
        $userProfile1 = new UserProfile();
        $userProfile2 = new UserProfile();
        $this->em->persist($userProfile1);
        $this->em->persist($userProfile2);
        $this->em->flush();

        $user->setUserProfile($userProfile1);
        $user2->setUserProfile($userProfile2);

        $this->em->flush();

        try{
            $this->em->persist($game);
            $this->em->flush();
        }
        catch (\Exception $exception){
            $this->assertStringContainsString('cannot be saved directly. Use the save method', $exception->getMessage());
        }
        $this->gameRepository->save($game, $user);
        $this->assertEquals('targi', $game->getName());
        $this->assertIsInt($game->getId());

        $play = new Play();
        $play->addUserProfile($user->getUserProfile());
        $play->addUserProfile($user2->getUserProfile());

        $this->playRepository->save($play);

        /**@var \Doctrine\Common\Collections\ArrayCollection $userProfiles*/
        $userProfiles = $play->getUserProfiles();
        $this->assertEquals(2, $userProfiles->count());
        $this->assertTrue($play->getUserProfiles()->contains($userProfile1));
        $this->assertTrue($userProfile1->getPlays()->contains($play));
    }
}