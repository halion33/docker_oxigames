<?php

namespace Tests\Application\Service\Game;

use App\Tests\BGGApiTestCase;

class ProcessCollectionServiceTest extends BGGApiTestCase
{
    protected $gameRepository;

    /**@var \Symfony\Component\Messenger\MessageBusInterface $bus */
    protected $bus;

    /**@var \Symfony\Component\Messenger\Transport\Doctrine\DoctrineTransport $doctrineTransport */
    private $doctrineTransport;

    private $processCollectionService;

    protected function setUp() : void
    {
        self::bootKernel();
        parent::setUp();
        $this->gameRepository = self::$container->get('OGO\Infrastructure\Persistence\Doctrine\GameRepository');
        $this->bus = self::$container->get('Symfony\Component\Messenger\MessageBusInterface');
        $this->doctrineTransport= self::$container->get('messenger.transport.async');
        $this->processCollectionService= self::$container->get('OGO\Application\Service\Game\ProcessCollectionService');
    }

    /** @test */
    public function importationSuccessTest(){
        $fakeXmlData = file_get_contents($this->getFakeDataDirectory() . '/bggCollection50OrLessChunk.xml');
        $xml = simplexml_load_string($fakeXmlData);
        $user = $this->createUser('oscar');

        $userGamesImportation = $this->processCollectionService->process($xml, $user);
        $this->assertEquals(42, $userGamesImportation->getTotalNumberGames());

    }

}