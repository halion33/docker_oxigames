<?php

namespace Tests\OGO\Application\MessageHandler;

use App\Tests\OGOTestCase;
use OGO\Application\MessageHandler\CollectionToImportMessageHandler;
use Doctrine\ORM\EntityManagerInterface;
use OGO\Application\Service\Game\CollectionByUserAPICall;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;
use OGO\Domain\Game\UserGamesImportation;
use OGO\Infrastructure\Persistence\Doctrine\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use OGO\Application\Message\CollectionToImportMessage;
use Symfony\Component\HttpFoundation\JsonResponse;
use OGO\Application\Service\Game\ProcessCollectionService;

class CollectionToImportMessageHandlerTest extends OGOTestCase
{
    /**@var GameRepository $gameRepository */
    private $gameRepository;

    /**@var EntityManagerInterface $em*/
    private $em;

    private $bggCollectionByUserAPICall;

    private $processCollectionService;

    protected function setUp(): void
    {
        self::bootKernel();
        parent::setUp();
        $this->gameRepository               = self::$container->get('OGO\Infrastructure\Persistence\Doctrine\GameRepository');
        $this->em                           = self::$container->get('Doctrine\ORM\EntityManagerInterface');
        $this->bggCollectionByUserAPICall   = self::$container->get('OGO\Application\Service\Game\CollectionByUserAPICall');
        $this->processCollectionService     = self::$container->get('OGO\Application\Service\Game\ProcessCollectionService');
    }

    /** @test */
    public function ImportCollectionTest()
    {
        $user = $this->createUser('oxieva');
        $collectionToImportMessage = new CollectionToImportMessage($user->getId());
        $fakeXmlData = file_get_contents($this->getFakeDataDirectory() . '/bggCollection.xml');
        $xml = simplexml_load_string($fakeXmlData);

        $userRepositoryMock = $this->getMockBuilder(UserRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $userRepositoryMock->expects($this->once())
            ->method('find')
            ->with($user->getId())
            ->willReturn($user);

        $gameRepositoryMock = $this->getMockBuilder( GameRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $collectionByUserAPICallMock = $this->getMockBuilder( get_class($this->bggCollectionByUserAPICall))
            ->onlyMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();
        $collectionByUserAPICallMock->expects($this->once())
            ->method('get')
            ->with($this->equalTo($user))
            ->will($this->returnValue($fakeXmlData));

        $processCollectionServiceMock = $this->getMockBuilder( get_class($this->processCollectionService))
            ->onlyMethods(['process'])
            ->disableOriginalConstructor()
            ->getMock();
        $processCollectionServiceMock->expects($this->once())
            ->method('process')
            ->with($this->equalTo($xml));

        $messageHandler = new CollectionToImportMessageHandler($gameRepositoryMock, $collectionByUserAPICallMock, $processCollectionServiceMock , $userRepositoryMock);
        $result = $messageHandler($collectionToImportMessage);
        $this->assertInstanceOf(UserGamesImportation::class, $result);
        //https://codeburst.io/unit-functional-test-the-symfony4-messenger-9eef328dce8
    }
}

