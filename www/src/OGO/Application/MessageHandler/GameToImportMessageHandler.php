<?php

namespace OGO\Application\MessageHandler;

use OGO\Application\Message\CollectionToImportMessage;
use OGO\Application\Message\GameToImportMessage;
use OGO\Application\Service\Game\BggGameByObjectIdAPICall;
use OGO\Application\Service\Game\CheckBggGameExists;
use Doctrine\ORM\EntityManagerInterface;
use OGO\Domain\Game\Game;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;
use OGO\Domain\User\User;
use OGO\Infrastructure\Persistence\Doctrine\UserGamesImportationRepository;
use OGO\Infrastructure\Persistence\Doctrine\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GameToImportMessageHandler implements MessageHandlerInterface
{
    private $gameRepository;

    private $userRepository;

    private $checkBggGameExists;

    private $bggGameByObjectIdAPICall;
    /**
     * @var UserGamesImportationRepository
     */
    private $userGamesImportationRepository;


    public function __construct(
        GameRepository $gameRepository,
        UserRepository $userRepository,
        CheckBggGameExists $checkBggGameExists,
        BggGameByObjectIdAPICall $bggGameByObjectIdAPICall,
        UserGamesImportationRepository $userGamesImportationRepository
    )
    {
        $this->gameRepository = $gameRepository;
        $this->userRepository = $userRepository;
        $this->checkBggGameExists = $checkBggGameExists;
        $this->bggGameByObjectIdAPICall = $bggGameByObjectIdAPICall;
        $this->userGamesImportationRepository = $userGamesImportationRepository;
    }

    /**
     * @param GameToImportMessage $message
     * @throws \Exception
     */
    public function __invoke(GameToImportMessage $message)
    {
        /**@var User $user*/
        $user = $this->userRepository->find($message->getUserId());
        if(!$user){
            throw new \Exception('User does not exists');
        }
        $gameVO = $message->getGame();

        $activeImport = $this->userGamesImportationRepository->getActiveImportationByUserId($user->getId());
        if(!$activeImport){
            throw new \Exception('User does not have an active importation of games');
        }

        $activeImport->incrementNumberOfGamesImported();

        $gameAlreadyExists = $this->checkBggGameExists->call($gameVO->getExternalId());
        if (!$gameAlreadyExists) {
            $gameEntity =new Game(
                $gameVO->getName(),
                $gameVO->getDescription(),
                $gameVO->getImage(),
                $gameVO->getExternalId(),
                $gameVO->getThumbnail()
            );
            $this->gameRepository->save($gameEntity, $user);
        }else{
            $user->addGame(reset($gameAlreadyExists));
            $this->userRepository->save($user);
        }

        if ( (isset($gameEntity) &&!$gameEntity instanceof Game) && !$gameAlreadyExists instanceof Game) {
            throw new \Exception('Logical error importing the game . GameToImportMessageHandler.');
        }
    }
}