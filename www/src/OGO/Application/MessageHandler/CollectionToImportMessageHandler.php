<?php

namespace OGO\Application\MessageHandler;

use OGO\Application\Message\CollectionToImportMessage;
use OGO\Application\Service\Game\CollectionByUserAPICall;
use OGO\Application\Service\Game\ProcessCollectionService;
use Doctrine\ORM\EntityManagerInterface;
use OGO\Domain\User\User;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;
use OGO\Infrastructure\Persistence\Doctrine\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CollectionToImportMessageHandler implements MessageHandlerInterface
{
    private $gameRepository;

    private $bggCollectionByUserApiCall;

    private $processCollectionService;

    private $userRepository;

    public function __construct(
        GameRepository $gameRepository,
        CollectionByUserAPICall $bggCollectionByUserAPICall,
        ProcessCollectionService $processCollectionService,
        UserRepository $userRepository)
    {
        $this->gameRepository = $gameRepository;
        $this->bggCollectionByUserApiCall = $bggCollectionByUserAPICall;
        $this->processCollectionService = $processCollectionService;
        $this->userRepository = $userRepository;
    }

    public function __invoke(CollectionToImportMessage $message)
    {
        /**@var User $user*/
        $user = $this->userRepository->find($message->getUserId());
        //@todo: check if there are an active importation for the user.
        $bggResponse = $this->bggCollectionByUserApiCall->get($user);
        $simpleXMLResponse = simplexml_load_string($bggResponse);
        return $this->processCollectionService->process($simpleXMLResponse, $user);
    }
}