<?php


namespace OGO\Application\Service\Game;

use OGO\Infrastructure\Persistence\Doctrine\GameRepository;

class CheckBggGameExists
{
    private $gameRepository;

    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function call($objectId){
        return $this->gameRepository->findByExternalId($objectId);
    }

}