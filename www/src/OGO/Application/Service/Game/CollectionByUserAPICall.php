<?php

namespace OGO\Application\Service\Game;

use OGO\Domain\Game\UserGamesImportation;
use OGO\Domain\Game\ValueObject\GameVO;
use OGO\Domain\User\User;
use OGO\Application\Shared\APIClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CollectionByUserAPICall
{
    /**
     * @var APIClient\APIClient
     */
    private $apiClient;

    private $logger;

    public function __construct(APIClient\APIClient $apiClient, LoggerInterface $logger)
    {
        $this->apiClient    = $apiClient;
        $this->logger       = $logger;
    }

    public function get(User $user): String
    {
        $collectionClient = $this->apiClient->getCollectionClient();
        /**@var \GuzzleHttp\Psr7\Response $bggResponse*/
        $bggResponse = $collectionClient->request('GET', $user->getBggIdentifier());
        $bggStringResponse = $bggResponse->getBody();
//        $this->logger->error('BGGRESPONSE');
//        $this->logger->error(var_dump($bggStringResponse->__toString()));
        return $bggStringResponse;
    }
}