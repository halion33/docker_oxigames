<?php
namespace OGO\Application\Service\Game;

use OGO\Application\Message\GameToImportMessage;
use OGO\Domain\Game\UserGamesImportation;
use OGO\Infrastructure\Persistence\Doctrine\UserGamesImportationRepository;
use OGO\Domain\Game\ValueObject\GameVO;
use OGO\Domain\User\User;

class ProcessCollectionService
{

    /**
     * @var UserGamesImportationRepository
     */
    private $userGamesImportationRepository;

    public function __construct(UserGamesImportationRepository $userGamesImportationRepository)
    {

        $this->userGamesImportationRepository = $userGamesImportationRepository;
    }

    public function process(\SimpleXMLElement $bggResponseBody, User $user) : UserGamesImportation
    {
        $items = $bggResponseBody->children();
        $newImportation = new UserGamesImportation();

        $newImportation->setTotalNumberGames($items->count());
        $newImportation->setUserId($user->getId());
        $newImportation->setName('Importation for user:'. $user->getBggIdentifier());

        foreach($items as $item){
            $name           = (string)$item->name;
            $description    = (string)$item->description;
            $thumbnail      = (string)$item->thumbnail;
            $image          = (string)$item->image;
            $externalId     = (int)$item->attributes()['objectid'];

            $gameToBeProcessed = new GameVO($name, $description, $thumbnail, $image, $externalId);

            $newImportation->record(new GameToImportMessage($gameToBeProcessed, $user->getId()));
        }

        if($items->count() > 0 ) {
            $this->userGamesImportationRepository->save($newImportation, $user);
        }

        return $newImportation;
    }


}