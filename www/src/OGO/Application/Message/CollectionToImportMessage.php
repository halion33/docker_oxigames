<?php

namespace OGO\Application\Message;

use OGO\Domain\User\User;

class CollectionToImportMessage
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

}