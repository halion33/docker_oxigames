<?php

namespace OGO\Application\Message;

use OGO\Domain\Game\ValueObject\GameVO;

class GameToImportMessage
{
    private $game;

    private $userId;

    public function __construct(GameVO $game, $userId)
    {
        $this->game = $game;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getGame(): GameVO
    {
        return $this->game;
    }
    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }
    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }
}