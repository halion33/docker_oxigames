<?php


namespace OGO\Application\Shared\APIClient;


use GuzzleHttp\ClientInterface;

interface APIClient
{
    public function getClient(): ClientInterface;
}