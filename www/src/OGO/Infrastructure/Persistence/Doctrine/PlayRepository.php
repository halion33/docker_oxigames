<?php
namespace OGO\Infrastructure\Persistence\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use OGO\Domain\Game\Play;
use OGO\Domain\User\User;
use OGO\Domain\Game\PlayRepositoryInterface;
use OGO\Domain\User\UserProfile;

class PlayRepository extends ServiceEntityRepository implements PlayRepositoryInterface
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Play::class);
    }

    public function find($id, $lock_mode = null, $lockVersion = null)
    {
        return $this->_em->find(Play::class, $id);
    }

    public function save(Play $play): void
    {
        $play->setEntityIsReadyToPersist(true);
        $this->_em->persist($play);
        $this->_em->flush();
    }

    public function remove(Play $play): void
    {
        $this->_em->remove($play);
        $this->_em->flush();
    }

}