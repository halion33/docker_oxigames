<?php
namespace OGO\Infrastructure\Persistence\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use OGO\Domain\User\User;
use OGO\Domain\Game\Game;
use OGO\Domain\Game\UserGamesImportation;

class GameCollectionRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function find($id, $lock_mode = null, $lockVersion = null)
    {
        return $this->_em->find(UserGamesImportation::class, $id);
    }

    public function save(UserGamesImportation $importation, User $user): void
    {
        //$game->record(new GameWasCreatedMessage($game, $user->getId()));
        $importation->setEntityIsReadyToPersist(true);
        $this->_em->persist($importation);
        $this->_em->flush();
    }

    public function remove(Game $game): void
    {
        $this->_em->remove($game);
        $this->_em->flush();
    }

}