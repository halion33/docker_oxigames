<?php
namespace OGO\Infrastructure\Persistence\Doctrine;

use OGO\Domain\Game\Message\GameWasCreatedMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use OGO\Domain\Game\Game;
use OGO\Domain\User\User;

class GameRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function find($id, $lock_mode = null, $lockVersion = null)
    {
        return $this->_em->find(Game::class, $id);
    }

    public function save(Game $game,User $user): void
    {
        $game->setEntityIsReadyToPersist(true);
        $this->_em->persist($game);
        $this->_em->flush();
        $game->record(new GameWasCreatedMessage($game->getId(), $user->getId()));
    }

    public function remove(Game $game): void
    {
        $this->_em->remove($game);
        $this->_em->flush();
    }

}