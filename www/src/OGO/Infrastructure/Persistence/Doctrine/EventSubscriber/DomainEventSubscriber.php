<?php

namespace OGO\Infrastructure\Persistence\Doctrine\EventSubscriber;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\EventSubscriber;
use OGO\Domain\Shared\EntityWithSaveMethodInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use OGO\Domain\Shared\ContainsEventsInterface;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;

final class DomainEventSubscriber implements EventSubscriber
{
    /**
     * @var MessageBusInterface
     */
    private $bus;
    /**
     * @var Collection
     */
    private $entities;

    /**
     * DomainEventSubscriber constructor.
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
        $this->entities = new ArrayCollection();
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'prePersist',
            'preUpdate',
            'preRemove',
            'preFlush',
            'postFlush'
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $this->addContainsEventsEntityToCollection($args);
    }
    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $this->addContainsEventsEntityToCollection($args);
    }
    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args): void
    {
        $this->addContainsEventsEntityToCollection($args);
    }
    /**
     * @param LifecycleEventArgs $args
     */
    private function addContainsEventsEntityToCollection(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof ContainsEventsInterface) {
            $this->entities->add($entity);
        }
    }

    /**
     * @param PreFlushEventArgs $args
     */
    public function preFlush(PreFlushEventArgs $args): void
    {
        $unitOfWork = $args->getEntityManager()->getUnitOfWork();
        $insertions = $unitOfWork->getScheduledEntityInsertions();
        $entities = [];
        foreach($insertions as $insertion){
            $entities[] = $insertion;
        }

        $updates = $unitOfWork->getScheduledEntityUpdates();
        foreach($updates as $update){
            $entities[] = $update;
        }

        $deletions = $unitOfWork->getScheduledEntityDeletions();
        foreach($deletions as $deletion){
            $entities[] = $deletion;
        }

        foreach ($entities as $entity) {
            if (!in_array(ContainsEventsInterface::class, class_implements($entity), true)) {
                continue;
            }
            if(in_array(EntityWithSaveMethodInterface::class, class_implements($entity), true) && $entity->getEntityIsReadyToPersist() ==false){
                throw new \Exception(sprintf('Entity %s cannot be saved directly. Use the save method on the repo', get_class($entity)), 400);
            }

            $this->entities->add($entity);
        }
    }

    /**
     * @param PostFlushEventArgs $args
     */
    public function postFlush(PostFlushEventArgs $args): void
    {
        $events = new ArrayCollection();
        foreach ($this->entities as $entity) {
            foreach ($entity->getRecordedEvents() as $domainEvent) {
                $events->add($domainEvent);
            }
            $entity->clearRecordedEvents();
        }
        foreach ($events as $event) {
            $this->bus->dispatch($event);
        }
    }

}