<?php
namespace OGO\Infrastructure\Persistence\ApiPlatform;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use OGO\Domain\Game\Game;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;
use OGO\Infrastructure\Persistence\Doctrine\UserRepository;
use Symfony\Component\Security\Core\Security;
use OGO\Domain\User\User;

final class GameDataPersister implements ContextAwareDataPersisterInterface
{
    private $security;

    private $em;

    public function __construct(Security $security, EntityManagerInterface $em)
    {
        $this->security = $security;
        $this->em = $em;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Game;
    }

    public function persist($data, array $context = [])
    {
        /**@var GameRepository $gameRepository*/
        $gameRepository = $this->em->getRepository(Game::class);
        /**@var UserRepository $userRepository*/
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findUserByUsername($this->security->getUser()->getUsername());
        $gameRepository->save($data, $user);
        return $data;
    }

    public function remove($data, array $context = [])
    {
// call your persistence layer to delete $data
    }
}