<?php


interface MailSenderInterface
{
    public function send(SendableInterface $message);
}