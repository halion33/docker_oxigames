<?php


interface SendableInterface
{
    public function getMessage(): string;
}