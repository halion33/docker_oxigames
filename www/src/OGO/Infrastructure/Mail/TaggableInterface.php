<?php


interface TaggableInterface
{
    public function getTags(): array;
}