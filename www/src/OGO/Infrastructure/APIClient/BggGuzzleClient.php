<?php

namespace OGO\Infrastructure\APIClient;

use GuzzleHttp;
use OGO\Application\Shared\APIClient\APIClient;

class BggGuzzleClient implements APIClient {

    private $client;

    private $collectionClient;

    public function __construct()
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => $_ENV['BGG_API_URL'],
            'http_errors' => false,
            'exceptions' => false,
            'verify' => false
        ]);
    }

    public function getClient() : GuzzleHttp\ClientInterface
    {
        return $this->client;
    }

    public function getCollectionClient() : GuzzleHttp\ClientInterface
    {
        if(!$this->collectionClient instanceof GuzzleHttp\Client) {
            $this->collectionClient = new GuzzleHttp\Client([
                'base_uri' => $_ENV['BGG_API_URL'],
                'http_errors' => false,
                'exceptions' => false,
                'verify' => false
            ]);
        }

        return $this->collectionClient;
    }

}