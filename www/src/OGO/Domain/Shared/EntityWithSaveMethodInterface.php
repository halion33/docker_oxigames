<?php

namespace OGO\Domain\Shared;

interface EntityWithSaveMethodInterface{

    public function getEntityIsReadyToPersist() : bool;

}
