<?php

namespace OGO\Domain\Shared;

interface RecordsEventsInterface
{
    public function record($event): void;
}