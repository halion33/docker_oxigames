<?php

namespace OGO\Domain\Shared;

interface ContainsEventsInterface
{
    public function getRecordedEvents(): array;

    public function clearRecordedEvents(): void;
}