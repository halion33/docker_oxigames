<?php

namespace OGO\Domain\Shared;

use Symfony\Component\Serializer\Annotation\Groups;

trait EventRecorderTrait
{
    /**
     * @var array
     */
    private $messages = [];
    /**
    * @Groups("write")
    */
    private $readyToPersist = false;

    public function getRecordedEvents(): array
    {
        return $this->messages;
    }

    public function clearRecordedEvents(): void
    {
        $this->messages = [];
    }

    public function record($message): void
    {
        $this->messages[] = $message;
    }

    public function getEntityIsReadyToPersist(): bool
    {
        return $this->readyToPersist;
    }

    public function setEntityIsReadyToPersist(bool $bool){
        $this->readyToPersist = $bool;
    }
}