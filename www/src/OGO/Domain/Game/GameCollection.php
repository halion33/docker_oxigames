<?php
namespace OGO\Domain\Game;

use Doctrine\ORM\Mapping as ORM;
use OGO\Domain\Shared\ContainsEventsInterface;
use OGO\Domain\Shared\EntityWithSaveMethodInterface;
use OGO\Domain\Shared\RecordsEventsInterface;
use OGO\Domain\User\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;
use OGO\Domain\Shared\EventRecorderTrait;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DateTime;

/**
 * Importation
 *
 * @ORM\Table(name="game_collection")
 * @ORM\Entity(repositoryClass="OGO\Infrastructure\Persistence\Doctrine\GameCollectionRepository")
 * @ApiResource()
 */
class GameCollection implements ContainsEventsInterface, RecordsEventsInterface, EntityWithSaveMethodInterface
{
    use EventRecorderTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $name;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="pending_imports", type="boolean")
     */
    private $pendingImports;

    /**
     * @var Game[]
     * @ORM\ManyToMany(targetEntity="Game", cascade={"persist"})
     * @ORM\JoinTable(name="collection_game",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")}
     * )
     */
    private $games;

    public function __construct()
    {
        $this->games = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return bool
     */
    public function isPendingImports(): bool
    {
        return $this->pendingImports;
    }

    /**
     * @param bool $pendingImports
     */
    public function setPendingImports(bool $pendingImports): void
    {
        $this->pendingImports = $pendingImports;
    }

    /**
     * @return Game[]
     */
    public function getGames(): array
    {
        return $this->games;
    }

    /**
     * @param Game[] $games
     */
    public function setGames(array $games): void
    {
        $this->games = $games;
    }

    public function addGame(Game $game): void
    {
        if(!$this->games->contains($game)) {
            $this->games->add($game);
        }
    }

    public function removeGame(Game $game): void
    {
        if($this->games->contains($game)) {
            $this->games->remove($game);
        }
    }

}