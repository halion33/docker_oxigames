<?php
namespace OGO\Domain\Game\ValueObject;

class GameVO implements \Serializable
{
    private $externalId;

    private $name;

    private $description;

    private $thumbnail;

    private $image;

    public function __construct(
        String  $name,
        String  $description,
        String  $thumbnail,
        String  $image,
        Int     $externalId
    )
    {
        $this->name           = $this->cleanString((string)$name);
        $this->description    = $this->cleanString((string)$description);
        $this->thumbnail      = $this->cleanString((string)$thumbnail);
        $this->image          = $this->cleanString((string)$image);
        $this->externalId     = $externalId;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        // TODO: Implement unserialize() method.
    }

    private function cleanString($string){
        $text = strip_tags($string);
        $cleanString = preg_replace("/&#?[a-z0-9];/i","",$text );
        $cleanString = join("", array_map("ltrim", explode("\n", $cleanString )));
        return $cleanString;
    }

    /**
     * @return Int
     */
    public function getExternalId(): Int
    {
        return $this->externalId;
    }

    /**
     * @param Int $externalId
     */
    public function setExternalId(Int $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|string[]|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|string[]|null $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|string[]|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string|string[]|null $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|string[]|null
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param string|string[]|null $thumbnail
     */
    public function setThumbnail($thumbnail): void
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return string|string[]|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string|string[]|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }


}