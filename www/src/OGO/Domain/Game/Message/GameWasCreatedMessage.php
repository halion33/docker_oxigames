<?php

namespace OGO\Domain\Game\Message;

use OGO\Domain\Game\ValueObject\GameVO;
use OGO\Domain\User\User;
use OGO\Domain\Game\Game;

class GameWasCreatedMessage
{
    private $gameId;

    private $userId;

    public function __construct(Int $gameId, Int $userId)
    {
        $this->gameId = $gameId;
        $this->userId = $userId;
    }

    /**
     * @return GameVO
     */
    public function getGameId(): Int
    {
        return $this->gameId;
    }


    /**
     * @return User
     */
    public function getUserId(): Int
    {
        return $this->userId;
    }
}