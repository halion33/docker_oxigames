<?php

namespace OGO\Domain\Game\MessageHandler;

use Doctrine\ORM\EntityManagerInterface;
use OGO\Domain\Game\Game;
use OGO\Infrastructure\Persistence\Doctrine\UserRepository;
use OGO\Domain\Game\Message\GameWasCreatedMessage;
use OGO\Infrastructure\Persistence\Doctrine\UserGamesImportationRepository;
use OGO\Infrastructure\Persistence\Doctrine\GameRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GameWasCreatedMessageHandler implements MessageHandlerInterface
{
    private $gameRepository;

    private $userRepository;

    private $em;
    /**
     * @var UserGamesImportationRepository
     */
    private $userGamesImportationRepository;

    public function __construct
    (
        GameRepository $gameRepository,
        UserRepository $userRepository,
        UserGamesImportationRepository $userGamesImportationRepository,
        EntityManagerInterface $entityManager
    ){
        $this->gameRepository   = $gameRepository;
        $this->userRepository   = $userRepository;
        $this->em               = $entityManager;
        $this->userGamesImportationRepository = $userGamesImportationRepository;
    }

    public function __invoke(GameWasCreatedMessage $message)
    {
        $user = $this->userRepository->find($message->getUserId());
        $gameId = $message->getGameId();
        $gameEntity = $this->gameRepository->find($gameId);
        if(is_null($gameEntity)) {
            throw new NotFoundHttpException('Game ' . $gameId . 'was no found');
        }
        $user->addGame($gameEntity);
        $this->em->flush();
    }
}