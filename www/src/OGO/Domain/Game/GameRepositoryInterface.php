<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 18/01/20
 * Time: 18:09
 */

namespace OGO\Domain\Game;


interface GameRepositoryInterface
{

    public function find(int $id);

    public function save(Game $game) : void;

    public function remove(Game $game) : void;

}