<?php
namespace OGO\Domain\Game;

use Doctrine\ORM\Mapping as ORM;
use OGO\Domain\Shared\ContainsEventsInterface;
use OGO\Domain\Shared\EntityWithSaveMethodInterface;
use OGO\Domain\Shared\EntityWithSaveMethodTrait;
use OGO\Domain\Shared\RecordsEventsInterface;
use OGO\Domain\User\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;
use OGO\Domain\Shared\EventRecorderTrait;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="OGO\Infrastructure\Persistence\Doctrine\GameRepository")
 * @UniqueEntity(
 *     fields={"externalId"},
 *     message="Existing game in BGG")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 */
class Game implements ContainsEventsInterface, RecordsEventsInterface, EntityWithSaveMethodInterface
{
    use EventRecorderTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $name;

    /**
     * @var string
     * @Groups({"read", "write"})
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "image/png", "image/jpg","image/jpeg" })
     */
    private $image;

    /**
     * @var string
     * @Groups({"read", "write"})
     * @ORM\Column(name="externalId", type="string", nullable=true, length=510)
     */
    private $externalId;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "image/png", "image/jpg", "image/gif", "image/jpeg" })
     */
    private $thumbnail;

    /**
     * Many Games have Many Users.
     * @ManyToMany(targetEntity="OGO\Domain\User\User", mappedBy="games")
     */
    private $users;

    public function __construct(
        $name = null,
        $description = null,
        $image = null,
        $externalId = null,
        $thumbnail = null
    )
    {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->externalId = $externalId;
        $this->thumbnail = $thumbnail;
        $this->users = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id
     *
     * @return Game
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Set name
     *
     * @param string $name
     *
     * @return Game
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getExternalId(){
        return $this->externalId;
    }

    public function setExternalID($externalId){
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param mixed $image
     */
    public function setThumbnail($image)
    {
        $this->thumbnail = $image;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addGame($this);
        }
        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeGame($this);
        }
        return $this;
    }

    public function serialize()
    {
        return array(
            'name'          => $this->getName(),
            'description'   => $this->getDescription(),
            'image'         => $this->getImage(),
            'thumbnail'     => $this->getThumbnail(),
            'externalId'    => $this->getExternalId(),
            'internalId'    => $this->getId()

        );
    }

    public function __toString()
    {
        return $this->getName();
    }
}

