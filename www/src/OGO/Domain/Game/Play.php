<?php

namespace OGO\Domain\Game;

use Doctrine\ORM\Mapping as ORM;
use OGO\Domain\Shared\ContainsEventsInterface;
use OGO\Domain\Shared\EntityWithSaveMethodInterface;
use OGO\Domain\Shared\RecordsEventsInterface;
use OGO\Domain\Shared\EventRecorderTrait;
use OGO\Domain\User\UserProfile;
use ApiPlatform\Core\Annotation\ApiResource;
use OGO\Domain\Game\PlayRepositoryInterface;

/**
 * Play
 *
 * @ORM\Table(name="play")
 * @ORM\Entity(repositoryClass="PlayRepositoryInterface")
 * @ApiResource(
 *     normalizationContext={"groups"={"read"}},
 *     denormalizationContext={"groups"={"write"}}
 * )
 */
class Play implements ContainsEventsInterface, RecordsEventsInterface, EntityWithSaveMethodInterface
{
    use EventRecorderTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="Rating", type="integer", nullable= true)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="Comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * One Play belongs to Many UserProfiles.
     * @ORM\ManyToMany(targetEntity="OGO\Domain\User\UserProfile", inversedBy="plays")
     * @ORM\JoinTable(name="users_plays")
     */
    private $userProfiles;

    public function __construct() {
        $this->userProfiles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return Play
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Play
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return mixed
     */
    public function getUserProfiles()
    {
        return $this->userProfiles;
    }

    /**
     * @param mixed $userProfiles
     */
    public function setUserProfiles($userProfiles): void
    {
        $this->userProfiles = $userProfiles;
    }

    public function addUserProfile(UserProfile $userProfile): self
    {
        if(!$this->userProfiles->contains($userProfile)){
            $this->userProfiles->add($userProfile);
            $userProfile->addPlay($this);
        }
        return $this;
    }

    public function removeUserProfile(UserProfile $userProfile): self
    {
        if($this->userProfiles->contains($userProfile)){
            $this->userProfiles->remove($userProfile);
            $userProfile->removePlay($this);
        }
        return $this;
    }
}

