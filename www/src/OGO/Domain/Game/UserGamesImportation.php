<?php
namespace OGO\Domain\Game;

use Doctrine\ORM\Mapping as ORM;
use OGO\Domain\Shared\ContainsEventsInterface;
use OGO\Domain\Shared\EntityWithSaveMethodInterface;
use OGO\Domain\Shared\RecordsEventsInterface;
use OGO\Domain\User\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;
use OGO\Domain\Shared\EventRecorderTrait;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use OGO\Domain\Game\ValueObject\GameVO;

/**
 * Importation
 *
 * @ORM\Table(name="importation")
 * @ORM\Entity(repositoryClass="OGO\Infrastructure\Persistence\Doctrine\UserGamesImportationRepository")
 * @ApiResource()
 */
class UserGamesImportation implements ContainsEventsInterface, RecordsEventsInterface, EntityWithSaveMethodInterface
{
    use EventRecorderTrait;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"read", "write"})
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="completed", type="boolean")
     *
     */
    private $completed = false;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_games", type="integer")
     */
    private $totalNumberGames = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_games_ok", type="integer")
     */
    private $numberGamesImported = 0;

    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isCompleted(): bool
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     */
    public function setCompleted(bool $completed): void
    {
        $this->completed = $completed;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getTotalNumberGames(): int
    {
        return $this->totalNumberGames;
    }

    /**
     * @param int $totalNumberGames
     */
    public function setTotalNumberGames(int $totalNumberGames): void
    {
        $this->totalNumberGames = $totalNumberGames;
    }

    /**
     * @return int
     */
    public function getNumberGamesImported(): int
    {
        return $this->numberGamesImported;
    }

    /**
     * @param int $numberGamesImported
     */
    public function setNumberGamesImported(int $numberGamesImported): void
    {
        $this->numberGamesImported = $numberGamesImported;
    }

    public function incrementNumberOfGamesImported(){
        $this->numberGamesImported = $this->numberGamesImported + 1;
        if($this->numberGamesImported == $this->totalNumberGames){
            $this->completed = true;
        }
    }

/*    public function getGamesVOs(): array
    {
        return $this->gamesVOs;
    }

    public function addGameVo($gameVo): int
    {
        if(!$this->gamesVOs->contains($gameVo)){
            $this->gamesVOs->add($gameVo);
            $this->setTotalNumberGames($this->getTotalNumberGames() +1);
        }
        return $this->getTotalNumberGames();
    }

    public function setGamesVOs(array $gamesVOs): void
    {
        $this->gamesVOs = $gamesVOs;
    }*/



}