<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 18/01/20
 * Time: 18:09
 */

namespace OGO\Domain\Game;


interface PlayRepositoryInterface
{

    public function find(int $id);

    public function save(Play $play) : void;

    public function remove(Play $play) : void;

}