<?php
namespace OGO\Domain\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup
 *
 * @ORM\Table(name="user_group")
 * @ORM\Entity()
 */
class UserGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \array
     *
     * @ORM\Column(name="group_users", type="array")
     */
    private $group_users;

    /**
     * @var \array
     *
     * @ORM\Column(name="group_admins", type="array")
     */
    private $group_admins;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set group_users
     *
     * @param \array $group_users
     *
     * @return UserGroup
     */
    public function setGroupUsers($group_users)
    {
        $this->group_users = $group_users;

        return $this;
    }

    /**
     * Get group_users
     *
     * @return \array
     */
    public function getGroupUsers()
    {
        return $this->group_users;
    }


    /**
     * Set admin users
     *
     * @param \array $group_admins
     *
     * @return UserGroup
     */
    public function setGroupAdmins($group_admins)
    {
        $this->group_admins = $group_admins;

        return $this;
    }

    /**
     * Get admin users
     *
     * @return \array
     */
    public function getGroupAdmins()
    {
        return $this->group_admins;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return UserGroup
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}

