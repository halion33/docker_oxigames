<?php
namespace OGO\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use OGO\Domain\Game\Play;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * UserProfile
 *
 * @ORM\Table(name="user_profile")
 * @ORM\Entity()
 */
class UserProfile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var array|null
     *
     * @ORM\Column(name="main_interests", type="json_array", nullable=true)
     */
    private $mainInterests;

    /**
     * @ORM\ManyToMany(targetEntity="OGO\Domain\Game\Play", mappedBy="usersProfile")
     */
    private $plays;

    public function __construct() {
        $this->plays = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return UserProfile
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mainInterests.
     *
     * @param array|null $mainInterests
     *
     * @return UserProfile
     */
    public function setMainInterests($mainInterests = null)
    {
        $this->mainInterests = $mainInterests;

        return $this;
    }

    /**
     * Get mainInterests.
     *
     * @return array|null
     */
    public function getMainInterests()
    {
        return $this->mainInterests;
    }

    /**
     * @return mixed
     */
    public function getPlays()
    {
        return $this->plays;
    }

    /**
     * @param mixed $plays
     */
    public function setPlays($plays): void
    {
        $this->plays = $plays;
    }

    public function addPlay(Play $play): self
    {
        if (!$this->plays->contains($play)) {
            $this->plays[] = $play;
            $play->addUserProfile($this);
        }
        return $this;
    }

    public function removePlay(Play $play): self
    {
        if ($this->plays->contains($play)) {
            $this->plays->removeElement($play);
            $play->removeUserProfile($this);
        }
        return $this;
    }

    public function __toString()
    {
        return (string) $this->getId();
    }
}
