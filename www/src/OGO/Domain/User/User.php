<?php
namespace OGO\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use ApiPlatform\Core\Annotation\ApiResource;
use OGO\Domain\Game\Game;
/**
 * @ORM\Entity(repositoryClass="OGO\Infrastructure\Persistence\Doctrine\UserRepository")
 * @ORM\Table(name="users")
 * @UniqueEntity(
 *     fields={"username"},
 *     message="Existing account")
 * @UniqueEntity(
 *     fields={"email"},
 *     message="Existing account")
 * @ApiResource()
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */

    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(name="bgg_id", type="string", length=255)
     */

    protected $bggIdentifier;

    /**
     * @var string
     * @Assert\Length(max=4096)
     */
    protected $plainPassword = '';

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $password;

    public function eraseCredentials()
    {
        return null;
    }

    /**
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * @ORM\Column(name="roles", type="json", length=224)
     */
    protected $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * One User has One Profile.
     * Cascade Remove (if the user is deleted, the profile is deleted automaticaly)
     * Cascade Persist (if a user is created, we don't need to persist the related NEW entity manually
     * @OneToOne(targetEntity="UserProfile", cascade={"remove","persist"})
     * @JoinColumn(name="profile_id", referencedColumnName="id")
     */
    private $userProfile;

    protected $salt;

    /**
     * Many Users have Many Groups.
     * @ManyToMany(targetEntity="OGO\Domain\Game\Game", inversedBy="users", cascade={"persist"})
     * @JoinTable(name="users_games")
     */
    private $games;

    public function __construct($username = '', $password = '', $salt='', array $roles = [])
    {
        $this->username = $username;
        $this->password = $password;
        $this->roles = $roles;
        $this->salt = random_bytes(10);
        $this->games = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->email;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return null
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return null
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->username = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->username;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->salt,
            $this->bggIdentifier
        ));
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return boolean
     */
    public function isEnabled(){
        return $this->enabled;
    }

    /**
     * @param string $bggIdentifier
     */
    public function setbggIdentifier(string $bggIdentifier)
    {
        $this->bggIdentifier = $bggIdentifier;
    }



    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->salt,
            $this->bggIdentifier
            ) = unserialize($serialized);
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * @param mixed $userProfile
     */
    public function setUserProfile($userProfile)
    {
        $this->userProfile = $userProfile;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
        }
        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getBggIdentifier(): ?string
    {
        return $this->bggIdentifier;
    }
}