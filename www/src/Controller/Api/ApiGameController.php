<?php

namespace App\Controller\Api;

use App\Controller\BaseController;
use OGO\Domain\User\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class ApiGameController extends ApiBaseController
{

    /**
     * @Route("api/game/list/{userId}")
     * @Method({"GET","POST"})
     * @Entity("user", expr="repository.find(userId)")
     */

    public function getGamesByUserId(User $user){
        $userGames = $user->getGames()->toArray();
        $serializedArray = [];
        foreach($userGames as $game){
            $serializedArray[] = $game->serialize();
        }
/*
        $games = [
            0 => [
                'Name' => 'Targi',
                'Image'=> 'image',
                'PublishedDate' => '01-01-1999',
                'Category' => 'Worker placement',
                'Rank' => 1,
                'Rating' => 10,
                'NumberOfPlayers' => '2',
                'RecommendedNumberOfPlayers' => '2',
                'BestNumberOfPlayers' => '2',
                'Mechanics' => 'Set collection',
                'Author' => 'Andreas Steiger'
            ],
             1 => [
                'Name' => 'Agricola',
                'Image'=> 'image',
                'PublishedDate' => '01-01-2000',
                'Category' => 'Worker placement',
                'Rank' => 2,
                'Rating' => 9.9,
                'NumberOfPlayers' => '4',
                'RecommendedNumberOfPlayers' => '2-4',
                'BestNumberOfPlayers' => '2',
                'Mechanics' => 'Set collection',
                'Author' => 'Uwe Rosenberg'
            ]
        ];*/

        return new JsonResponse($serializedArray, 200);
    }



}