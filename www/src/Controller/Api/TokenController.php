<?php
namespace App\Controller\Api;

use App\Controller\BaseController;
use OGO\Domain\User\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class TokenController extends BaseController
{
    private $userPasswordEncoder;

    private $lexikJWTEncoder;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder, JWTEncoderInterface $JWTEncoder, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->lexikJWTEncoder = $JWTEncoder;
    }

    /**
     * @Route("api/token")
     * @Method({"GET", "POST"})
     */
    public function newTokenAction(RequestStack $requestStack)
    {
        $request = $requestStack->getMasterRequest();
        $isValid = false;

        $json_payload = json_decode($request->getContent(), true);

        if($json_payload && isset($json_payload['email'])) {
            $email = $json_payload['email'];
            $user = $this->getDoctrine()
                ->getRepository(User::class)
                ->findOneBy(['email' => $email]);
            if (!$user) {
                return new JsonResponse(['token' => 'wrong credentials']);
            } else {
                $password = $json_payload['password'];
            }
        }

        if(!isset($user)){
            return new JsonResponse(['token' => 'wrong credentials']);
        }

        if ($user instanceof User && isset($password)){
            $isValid = $this->userPasswordEncoder->isPasswordValid($user, $password);
        }elseif($user instanceof User){
            $isValid = $this->userPasswordEncoder->isPasswordValid($user, $request->getPassword());
        }
        if (!$isValid) {
            return new JsonResponse(['Bad Credentials'], 401);
        }

        //create token
        $token = $this->lexikJWTEncoder
            ->encode([
                'email' => $user->getEmail(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);

        $userEmail              = $user->getEmail();
        $userName               = $user->getName();
        $userBGGIdentifier      = $user->getBggIdentifier();
        $userId                 = $user->getId();


        return new JsonResponse(
            [
                'token'         => $token,
                'email'         => $userEmail,
                'name'          => $userName,
                'bggIdentifier' => $userBGGIdentifier,
                'id'            => $userId
            ]
        );
    }

    /**
     * @Route("api/refresh/token")
     * @Method({"GET", "POST"})
     */
    public function refreshTokenAction(RequestStack $requestStack, Security $security)
    {
        $request = $requestStack->getCurrentRequest();
        $authorizationHeader = $request->headers->get('Authorization');
        $user = $security->getUser();

        $token = $this->lexikJWTEncoder
            ->encode([
                'email' => $user->getEmail(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);

        return new JsonResponse(['token' => $token]);
    }

    /**
     * @Route("api/hello")
     * @Method("GET")
     */
    public function sayHello(Security $security){
        var_dump($security->getUser());exit();
        var_dump('hello'); exit();
    }
}