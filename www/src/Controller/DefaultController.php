<?php
namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    /**
     * @Route("/website/")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('Default/index.html.twig');
    }
    /**
     * @Route("/test")
     * @return Response
     */
    public function testAction()
    {
        var_dump('test'); exit();
        return $this->render('Default/index.html.twig');
    }


}