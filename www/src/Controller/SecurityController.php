<?php
namespace App\Controller;

use OGO\Infrastructure\User\Form\LoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/*use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;*/
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use OGO\Infrastructure\User\EventListener\HashPasswordListener;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route("/login", name="security_login")
     */


    public function loginAction(AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $form = $this->createForm(LoginForm::class, [
            'email' => $lastUsername
        ]);


        return $this->render(
            'Security/login.html.twig',
            array(
                // last username entered by the user
                'form' => $form->createView(),
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {
        throw new \Exception('this should not be reached!');
    }
}