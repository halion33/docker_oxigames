<?php
namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use OGO\Domain\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends EasyAdminController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    protected $em;

    /**
     * UserController constructor.
     *
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
    }

    public function persistEntity($entity)
    {
        $this->encodePassword($entity);
        parent::persistEntity($entity);
        if($entity instanceof User){
            //$newProfile = new Profile();
            //$em->persist($newProfile);
            //$entity->setProfile($newProfile);
            $this->em->flush();
        }

    }

    public function updateEntity($entity)
    {
        if(!empty($entity->getPassword())) {
            $this->encodePassword($entity);
        }else{
            $uow = $this->em->getUnitOfWork();
            $uow->computeChangeSets();
            $changeset = $uow->getEntityChangeSet($entity);

            if(isset($changeset['password'])){
                $entity->setPassword($changeset['password'][0]);
            }
        }

        parent::updateEntity($entity);
    }

    public function encodePassword($user)
    {
        if (!$user instanceof User) {
            return;
        }

        $user->setPassword(
            $this->passwordEncoder->encodePassword($user, $user->getPassword())
        );
    }
}