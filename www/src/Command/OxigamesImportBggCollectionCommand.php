<?php

namespace App\Command;

use OGO\Domain\User\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use OGO\Application\Message\CollectionToImportMessage;

class OxigamesImportBggCollectionCommand extends Command
{

    protected static $defaultName = 'oxigames:import-bgg-collection';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $bus;

    public function __construct(EntityManagerInterface $entityManager,  MessageBusInterface $bus)
    {
        $this->entityManager = $entityManager;
        $this->bus = $bus;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('BggUserIdentifier', InputArgument::REQUIRED, 'Bgg user id')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $userIdentifier = $input->getArgument('BggUserIdentifier');

        if ($userIdentifier) {
            $io->note(sprintf('You passed an argument: %s', $userIdentifier));
        }

        $user = $this->entityManager->getRepository(User::class)->findOneBy(['bggIdentifier' => $userIdentifier]);

        if(!$user){
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $userIdentifier]);
        }

        if(!$user){
            $io->error(sprintf('No user found with %s as external identifier', $userIdentifier));
            throw new  \Exception('User does not exist');
        }

        $this->bus->dispatch(new CollectionToImportMessage($user->getId()));

        $io->success('User collection is ready to been imported.');

        return 0;
    }
}
