### Online Game Organizer ###

Project to learn DDD, TDD, CQRS working on a distributed infrastructure. <3 Messenger

Using the BGG API we are going to import the user games collection.


**Backend** : 

![Image](https://app.bs4.oxiworks.xyz/var/usersFiles/oscar/image-1599471630314.png)

1. Symfony 5 + Apache
2. Mysql
3. Redis
4. Elastic stack

https://bitbucket.org/halion33/docker_oxigames/


**Frontend** :

![Image](https://app.bs4.oxiworks.xyz/var/usersFiles/oscar/image-1599479385873.jpg)

1. React Native

Private repo: https://bitbucket.org/halion33/oxigamesreactnative

**Infrastructure repository**:
Private repo: https://bitbucket.org/halion33/sf_docker

**Documentation in our documentation system**: 
Private documentation: https://app.bs4.oxiworks.xyz
Open infrastrucure documentation: https://app.bs4.oxiworks.xyz/doc/preparing-a-docker-system
